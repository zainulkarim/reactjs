installing ReactJs
npm install
#installing global package
npm install -g babel
npm install -g babel-cli
#installing dependencies and plugins
#--save means add these packages to package.json
npm install webpack --save
npm install webpack-dev-server --save
npm install react --save
npm install react-dom --save
npm install babel-core
npm install babel-loader
npm install babel-preset-react
npm install babel-preset-es2015
npm install webpack-dev-server -g
#create the files
touch index.html
touch App.jsx
touch main.js
touch webpack.config.js
#change package.json
delete test and change with this
"start": "webpack-dev-server --hot"
#-- hot means autoreload after changing file
#copy and paste this to webpack.config.js
var config = {
   entry: './main.js',
   output: {
      path:'/',
      filename: 'index.js',
   },
   devServer: {
      inline: true,
      port: 8080
   },
   module: {
      loaders: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
               presets: ['es2015', 'react']
            }
         }
      ]
   }
}
module.exports = config;

#for more
https://www.tutorialspoint.com/reactjs/reactjs_environment_setup.htm